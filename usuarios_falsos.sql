-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-07-2020 a las 21:55:46
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `usuarios_falsos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intentos_de_login`
--

CREATE TABLE `intentos_de_login` (
  `Id_intento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `intentos` tinyint(11) NOT NULL DEFAULT 0,
  `ultimo_intento` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intervalo_expiracion_contrasena`
--

CREATE TABLE `intervalo_expiracion_contrasena` (
  `dias_expiracion` smallint(90) NOT NULL DEFAULT 90
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(50) NOT NULL,
  `direccion_usuario` varchar(100) NOT NULL,
  `telefono_usuario` varchar(10) NOT NULL,
  `email_usuario` varchar(100) NOT NULL,
  `contrasena` varchar(255) NOT NULL,
  `hora_de_registro` datetime NOT NULL DEFAULT current_timestamp(),
  `contraseña_activa` bit(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre_usuario`, `direccion_usuario`, `telefono_usuario`, `email_usuario`, `contrasena`, `hora_de_registro`, `contraseña_activa`) VALUES
(1, 'Emilio', 'Venezuela 1991, Ex Hipódromo, 32330 Cd Juárez, Chih., México', '6562993305', 'usuario@hotmail.com', '321', '2020-06-27 10:51:08', b'00'),
(2, 'Johan', 'Calle falsa 123', '6561234567', 'alguien@hotmail.com', '123', '2020-06-27 10:54:25', b'00'),
(5, 'Joshua', 'Calle1', '6562993305', 'algo@hotmail.com', '$2y$10$Vngib1KO7oBjLuO8gl9im.aXsuILYW8yil1aANvWo13CWzW2AAWu.', '2020-06-27 12:07:19', b'00'),
(6, 'san pedro', 'El cielo', '6562993305', 'sanpedro@hotmail.com', '$2y$10$31.QqWyZZOnFix7NUOXzFelLjD.afV1rFpznzvx.I6/fSXewZWnXW', '2020-06-29 14:42:46', b'00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
