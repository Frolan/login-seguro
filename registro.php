<?php
require_once 'conexion.php';
session_start();

$failure = false;

print_r ($_POST);
if(isset($_POST['registrar'])){
	if( (trim($_POST['nombre_usuario']) !== '') && (trim($_POST['dirrecion_usuario']) !== '') && (trim($_POST['telefono_usuario']) !== '') && (trim($_POST['email_usuario']) !== '') && (trim($_POST['contrasena']) !== '') ){
		try{
			$nombre_usuario = trim($_POST['nombre_usuario']);
			$dirrecion_usuario = trim($_POST['dirrecion_usuario']);
			$telefono_usuario = trim($_POST['telefono_usuario']);
			$email_usuario = trim($_POST['email_usuario']);
			$contrasena = trim($_POST['contrasena']);
			
			//cifrar contrasena
			$contrasena_c = password_hash($contrasena, PASSWORD_DEFAULT);
			
			$qry = "INSERT INTO usuarios
							(nombre_usuario,direccion_usuario,telefono_usuario,email_usuario,contrasena)
							VALUES(:nombre, :direccion, :telefono, :email, :contrasena)
							";
			$stmt = $link->prepare($qry);
			$stmt->execute(array(
					':nombre' => $nombre_usuario,
					':direccion' => $dirrecion_usuario,
					':telefono' => $telefono_usuario,
					':email' => $email_usuario,
					':contrasena' => $contrasena_c
					)
				);
			header('Location:login.php');
			return;
		
		}catch(Exepcion $ex){
			echo 'Hubo un error';
			echo $ex->getMessage();
			return;
		}
		
		
	}else{
		$failure = 'Favor de ingresar todos los datos';
	}
}		
?>
<html>
<head>

	<meta charset="utf-8">
	<title>Login</title>

</head>
<body>

<?php
	if($failure !== false){
		echo $failure;
	}
?>

	<form method="POST">
		<label>Nombre: </label>
		<input type="text" name="nombre_usuario">
			<br>
		<label>Direccion: </label>
		<input type="text" name="dirrecion_usuario">
			<br>
		<label>Telefono: </label>
		<input type="text" name="telefono_usuario">
			<br>			
		<label>Correo: </label>
		<input type="email" name="email_usuario">
			<br>
		<label>Contrasena: </label>
		<input type="password" name="contrasena">	
			<br>
		<input type="submit" name="registrar" value="Registrar">
	</form>	
	
</body>
</html>