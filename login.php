<?php
require_once 'conexion.php';
session_start();

if(isset($_POST['acceder'])){
	if( (trim($_POST['correo_ingresado']) !== '') && (trim($_POST['contraseña_ingresada']) !== '') ){
			try{
				$correo = trim($_POST['correo_ingresado']);
				$contraseña = trim($_POST['contraseña_ingresada']);
				
				$qry = "SELECT * FROM usuarios WHERE email_usuario = :correo LIMIT 1";
				$stmt = $link->prepare($qry);
				$stmt->execute(array(
						':correo' => $correo,
						)
					);
					
				$res = $stmt->fetch(PDO::FETCH_ASSOC);
				$contrasena = $res['contrasena'];
				
					if (password_verify($contraseña, $contrasena)){
						$_SESSION['success'] = "¡Autenticacion exitosa";
					}
					else{
						//obtener el id del usuario NO ENVIA EL CORREO
						$con = "SELECT id_usuario FROM usuarios WHERE email_usuario = :correo LIMIT 1";
							$stmt1 = $link->prepare($con);
							$stmt1->execute(array(
									':correo' => $correo,
									)
								);
						$res1 = $stmt1->fetch(PDO::FETCH_ASSOC);
						$id_usuario = $res1['id_usuario'];
						
						//obtener los intentos de un id especifico
						$bus = "SELECT intentos FROM intentos_de_login WHERE id_usuario = :id_usuario ";
							$stmt2 = $link->prepare($bus);
							$stmt2->execute(array(
									':id_usuario' => $id_usuario
									)
								);
						$res2 = $stmt2->fetch(PDO::FETCH_ASSOC);	
						$intentos_fallidos = $res2['intentos'];
						
						
						
					/*	if($intentos_fallidos !== 0){
							$agregar = "INSERT INTO intentos_de_login (id_usuario, intentos)
										VALUES (:id, :intento)
										";
							$stmt3 = $link->prepare($agregar);
							$stmt3->execute(array(
									':id_usuario' => $id_usuario,
									':intento' => 1
									)
								);							
						} */
				
						if($intentos_fallidos < 3){
								
								///volvemos a verificar
								if($intentos_fallidos < 3){
									$_SESSION['error'] = "¡Autenticacion fallo!";
									
									$update = 	'UPDATE intentos_de_login SET 
													intentos = :intentos 				
												WHERE
													id_usuario = :id';
									$stmt4 = $link->prepare($update);
									$stmt4->execute(array(
											
											':id' => $id_usuario,
											)
										);
									
								}
								if ($intentos_fallidos > 3){
									UNSET($_SESSION['error']);
									$_SESSION['error'] = "¡Intentos maximos alcanzados!";
								}
							 
								/*	$update = 	'update intentos_de_login SET 
													intentos = :intentos 				
												WHERE
													id_usuario = :id';
									$stmt = $link->prepare($update);
									$stmt->execute(array(
											':correo' => $correo,
											)
										);
										
									$res = $stmt->fetch(PDO::FETCH_ASSOC);	 */				
							
							usleep(500000);///0.5s
						}			
					}	
			}catch(Exepcion $ex){
						echo 'Hubo un error';
						echo $ex->getMessage();
						return;
					}			
			
		header('Location:login.php');
		return;
		
	}else{
		$_SESSION['error'] = "Favor de llenar todos los campos";
	}
}
?>
<html>
<head>

	<meta charset="utf-8">
	<title>Login</title>

</head>
<body>
		<?php	
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			if( isset($_SESSION['error']) ){
				echo '<p style="color:red;">'.htmlentities($_SESSION['error']).'</p>';
				unset($_SESSION['error']);
				
				
			}
			echo $id_usuario;		
			
		?>
	<form method="POST">
		<label>Correo: </label>
		<input type="email" name="correo_ingresado">
			<br>
		<label>Contraseña: </label>
		<input type="password" name="contraseña_ingresada">	
			<br>
		<input type="submit" name="acceder" value="Ingresar">
	</form>		
</body>
</html>